package ${packageBase}.config;

import com.github.wzclouds.boot.config.BaseConfig;
import org.springframework.context.annotation.Configuration;
import com.github.wzclouds.oauth.api.LogApi;
import com.github.wzclouds.log.event.SysLogListener;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;

/**
 * ${description}-Web配置
 *
 * @author ${author}
 * @date ${date}
 */
@Configuration
public class ${service}WebConfiguration extends BaseConfig {

    /**
    * wzclouds.log.enabled = true 并且 wzclouds.log.type=DB时实例该类
    *
    * @param logApi
    * @return
    */
    @Bean
    @ConditionalOnExpression("${r'${'}wzclouds.log.enabled:true${r'}'} && 'DB'.equals('${r'${'}wzclouds.log.type:LOGGER${r'}'}')")
    public SysLogListener sysLogListener(LogApi logApi) {
        return new SysLogListener((log) -> logApi.save(log));
    }
}
