import com.github.wzclouds.generator.CodeGenerator;
import com.github.wzclouds.generator.config.CodeGeneratorConfig;
import com.github.wzclouds.generator.config.FileCreateConfig;
import com.github.wzclouds.generator.type.EntityFiledType;
import com.github.wzclouds.generator.type.EntityType;
import com.github.wzclouds.generator.type.GenerateType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 测试代码生成权限系统的代码
 *
 * @author wzclouds
 * @date 2019/05/25
 */
public class TestAuthorityGenerator {
    /***
     * 注意，想要在这里直接运行，需要手动增加 mysql 驱动
     * @param args
     */
    public static void main(String[] args) {
//        CodeGeneratorConfig build = buildDefaultsEntity();
//        CodeGeneratorConfig build = buildAuthSuperEntity();
//        CodeGeneratorConfig build = buildAuthEntity();
//        CodeGeneratorConfig build = buildCommonEntity();
//        CodeGeneratorConfig build = buildCommonSuperEntity();
//        CodeGeneratorConfig build = buildCoreEntity();
        CodeGeneratorConfig build = buildOrderEntity();

        build.setUsername("root");
        build.setPassword("6162659");
        System.out.println("输出路径：");
        System.out.println(System.getProperty("user.dir") + "/wzclouds-authrity");
        build.setProjectRootPath(System.getProperty("user.dir") + "/wzclouds-authrity");

        // null 表示 使用下面的 生成策略
        FileCreateConfig fileCreateConfig = new FileCreateConfig(null);
        // 不为null 表示忽略下面的 生成策略
//        FileCreateConfig fileCreateConfig = new FileCreateConfig(GenerateType.OVERRIDE);

        //实体类的生成策略 为覆盖
        fileCreateConfig.setGenerateEntity(GenerateType.OVERRIDE);
        fileCreateConfig.setGenerateEnum(GenerateType.OVERRIDE);
        fileCreateConfig.setGenerateDto(GenerateType.OVERRIDE);
        fileCreateConfig.setGenerateXml(GenerateType.OVERRIDE);
        //dao 的生成策略为 忽略
        fileCreateConfig.setGenerateDao(GenerateType.OVERRIDE);
        fileCreateConfig.setGenerateServiceImpl(GenerateType.OVERRIDE);
        fileCreateConfig.setGenerateService(GenerateType.OVERRIDE);
        fileCreateConfig.setGenerateController(GenerateType.OVERRIDE);
        build.setFileCreateConfig(fileCreateConfig);

        //手动指定枚举类 生成的路径
        Set<EntityFiledType> filedTypes = new HashSet<>();
        filedTypes.addAll(Arrays.asList(
                EntityFiledType.builder().name("httpMethod").table("c_common_opt_log")
                        .packagePath("com.github.wzclouds.common.enums.HttpMethod").gen(GenerateType.IGNORE).build()
                , EntityFiledType.builder().name("httpMethod").table("c_auth_resource")
                        .packagePath("com.github.wzclouds.common.enums.HttpMethod").gen(GenerateType.IGNORE).build()
                , EntityFiledType.builder().name("dsType").table("c_auth_role")
                        .packagePath("com.github.wzclouds.database.mybatis.auth.DataScopeType").gen(GenerateType.IGNORE).build()
        ));
        build.setFiledTypes(filedTypes);
        CodeGenerator.run(build);
    }

    public static CodeGeneratorConfig buildOrderEntity() {
        List<String> tables = Arrays.asList(
                "c_auth_user"
        );
        CodeGeneratorConfig build = CodeGeneratorConfig.
                build("authrity", "authrity", "wzclouds", "c_", tables);
        build.setSuperEntity(EntityType.ENTITY);
        build.setChildPackageName("user");
        build.setUrl("jdbc:mysql://106.54.254.212:3306/wzclouds_base?serverTimezone=CTT&characterEncoding=utf8&useUnicode=true&useSSL=false&autoReconnect=true&zeroDateTimeBehavior=convertToNull");
        return build;
    }
}
